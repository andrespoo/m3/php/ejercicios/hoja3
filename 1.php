<html>
    <head>
        <meta charset="UTF-8">
        <title>Seleccion</title>
    </head>
    <body>
        <h1>I.Seleccion IF</h1>

        <div>
            <form method="post">
                <table border="0">
                    <div><tr><td><label>Ingrese primer numero</label></td><td> <input name="num1" type="number"></td></tr></div>
                    <div><tr><td><label>Ingrese segundo numero</label></td><td> <input name="num2" type="number"></td></tr></div>
                    <div><tr><td><input type="submit" name="send" value="Enviar" /></td></tr></div>
                </table>
            </form>
        </div>
        <div>------------------------------------------------------------------------</div>
        <?php
        if (isset($_POST['send'])) {
            $numero1 = 0;
            $numero2 = 0;
            $salida = '';

            $numero1 = $_REQUEST['num1'];
            $numero2 = $_REQUEST['num2'];

            if ($numero1 > $numero2) {
                $salida = "tipo 1";
            } elseif ($numero1 == $numero2) {
                $salida = "tipo 2";
            } else {
                $salida = "tipo 3";

                if ($numero2 > 10) {
                    $salida = "tipo 31";
                } else {
                    $salida = "tipo 32";
                }
            }
            echo $salida;
        }
        ?>
    </body>
</html>
