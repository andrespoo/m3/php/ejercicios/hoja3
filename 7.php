<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <h1>VII.Bucle while</h1>
        <?php
        $c = 1;
        while ($c <= 100) {
            ?>
            <div>
                <?= $c; ?>
            </div>
            <?php
            $c++;
        }
        ?>
    </body>
</html>
