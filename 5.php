<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>bucle while</title>
    </head>
    <body>
        <h1>IV.Bucle while</h1>

        <?php
        $c = 0;
        ?>
        <ul>
            <?php
            $c = 1;
            while ($c <= 10) {
                ?>
                <li><?= $c; ?></li>

                <?php
                $c++;
            }
            ?>
        </ul>
    </body>
</html>
